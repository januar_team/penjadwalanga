<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPengampuAddUniqueConstraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengampus', function (Blueprint $table) {
            $table->unique(['lecturer_id','course_id','class_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengampus', function (Blueprint $table) {
            $table->dropUnique('pengampus_lecturer_id_course_id_class_id_unique');
        });
    }
}
