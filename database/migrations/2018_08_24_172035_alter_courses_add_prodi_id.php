<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCoursesAddProdiId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('courses', 'prodi_id')) {
            Schema::table('courses', function (Blueprint $table) {
                $table->dropColumn('prodi_id');
            });
        }
        Schema::table('courses', function (Blueprint $table) {
            $table->integer('prodi_id')->unsigned()->nullable()->after('id');
            $table->foreign('prodi_id')->references('id')->on('prodis')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->dropForeign('courses_prodi_id_foreign');
            $table->dropColumn('prodi_id');
        });
    }
}
