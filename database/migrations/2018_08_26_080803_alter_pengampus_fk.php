<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPengampusFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengampus', function (Blueprint $table) {
            $table->dropForeign('pengampus_lecturer_id_foreign');
            $table->foreign('lecturer_id')->references('id')->on('lecturers')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->dropForeign('pengampus_course_id_foreign');
            $table->foreign('course_id')->references('id')->on('courses')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->dropForeign('pengampus_class_id_foreign');
            $table->foreign('class_id')->references('id')->on('classes')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengampus', function (Blueprint $table) {
            $table->dropForeign('pengampus_lecturer_id_foreign');
            $table->foreign('lecturer_id')->references('id')->on('lecturers');

            $table->dropForeign('pengampus_course_id_foreign');
            $table->foreign('course_id')->references('id')->on('courses');

            $table->dropForeign('pengampus_class_id_foreign');
            $table->foreign('class_id')->references('id')->on('classes');
        });
    }
}
