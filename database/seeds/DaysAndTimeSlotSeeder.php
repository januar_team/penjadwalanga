<?php

use Illuminate\Database\Seeder;

class DaysAndTimeSlotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* generate days data */
        for ($i = 0; $i < 5; $i ++){
            if (!\App\Model\Day::where('id', $i + 1)->first()){
                $day = new \App\Model\Day();
                $day->fill([
                    'id' => $i + 1,
                    'name' => \Carbon\Carbon::now()->modify("this week +$i days")->formatLocalized("%A")
                ]);
                $day->save();
            }
        }

        /* generate time slot */
        $start = \Carbon\Carbon::createFromTime(8, 0, 0);
        for ($i = 0; $i < 6; $i++){
            if (!\App\Model\TimeSlot::where('start', $start->format("H:i"))->first()){
                $time = new \App\Model\TimeSlot();
                $time->fill([
                    'start' => $start->format("H:i"),
                    'end' => $start->copy()->addMinute(50)->format("H:i")
                ]);
                $time->save();
            }
            $start->addMinute(50);
        }

        $start = \Carbon\Carbon::createFromTime(16, 30, 0);
        for ($i = 0; $i < 6; $i++){
            if (!\App\Model\TimeSlot::where('start', $start->format("H:i"))->first()){
                $time = new \App\Model\TimeSlot();
                $time->fill([
                    'start' => $start->format("H:i"),
                    'end' => $start->copy()->addMinute(50)->format("H:i")
                ]);
                $time->save();
            }
            $start->addMinute(50);
        }
    }
}
