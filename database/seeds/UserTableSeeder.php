<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!DB::table('users')->where('username', 'admin')->first()){
            DB::table('users')->insert([
                'username' => 'admin',
                'name' => 'Administrator',
                'email' => 'admin@email.com',
                'password' => bcrypt('admin2121')
            ]);
        }
    }
}
