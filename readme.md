

## Requirements
- [PHP >= 7.1.3](http://php.net/)
- [Laravel 5.6](https://github.com/laravel/framework)
- Composer(https://getcomposer.org)

## Installation

#### Installing Composer
Please run command below in terminal(Linux)
```bash
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

#### Install Composer in Windows
Please download and install Composer-Setup.exe. Please download and read instruction from this link(https://getcomposer.org/doc/00-intro.md#installation-windows) 

#### Clone Project (using git command line - Windows)
Open git bash terminal or windows cammand line (cmd). After that, please run command below
```bash
cd <your-project-folder>
git clone https://bitbucket.org/januar_team/penjadwalanga.git
cd penjadwalanga
composer install
```
#### Create Database
First, create database using your database IDE (like phpmyadmin, sqlyog, mysql workbench).
Or
Run query below
```query
CREATE DATABASE /*!32312 IF NOT EXISTS*/`penjadwalan_ga` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
```

#### Change database config
Open the file config/database.php and then change database.

#### Migration and Laravel Key
Run command below to create laravel key
```bash
php artisan key:generate
```

Run command below to run database migration and seeder
```bash
php artisan migrate
php artisan db:seeder
```

Run server from command line, run di command
```bash
php artisan serve
```

Open yout browser and access the website from url http://localhost:800