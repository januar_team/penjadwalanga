<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function (){
    Route::get('/', 'HomeController@index')->name('dashboard');
    Route::get('/home', 'HomeController@index');
    Route::get('/home/day', 'HomeController@day')->name('home.day');
    Route::get('/home/time', 'HomeController@time')->name('home.time');

    /* prodi */
    Route::get('/prodi', 'ProdiController@index')->name('prodi');
    Route::match(['post', 'get'],'/prodi/add', 'ProdiController@add')->name('prodi.add');
    Route::match(['post', 'get'],'/prodi/edit/{id}', 'ProdiController@edit')->name('prodi.edit');
    Route::get('/prodi/delete/{id}', 'ProdiController@delete')->name('prodi.delete');

    /* lecturer */
    Route::get('/lecturer', 'LecturerController@index')->name('lecturer');
    Route::match(['post', 'get'],'/lecturer/add', 'LecturerController@add')->name('lecturer.add');
    Route::match(['post', 'get'],'/lecturer/edit/{id}', 'LecturerController@edit')->name('lecturer.edit');
    Route::get('/lecturer/delete/{id}', 'LecturerController@delete')->name('lecturer.delete');

    /* course */
    Route::get('/course', 'CourseController@index')->name('course');
    Route::match(['post', 'get'],'/course/add', 'CourseController@add')->name('course.add');
    Route::match(['post', 'get'],'/course/edit/{id}', 'CourseController@edit')->name('course.edit');
    Route::match(['post', 'get'],'/course/pengampu/{id}', 'CourseController@pengampu')->name('course.pengampu');
    Route::get('/course/delete/{id}', 'CourseController@delete')->name('course.delete');
    Route::get('/course/pengampu/{course}/delete/{id}', 'CourseController@pengampu_delete')->name('course.pengampu.delete');

    /* room */
    Route::get('/room', 'RoomController@index')->name('room');
    Route::match(['post', 'get'],'/room/add', 'RoomController@add')->name('room.add');
    Route::match(['post', 'get'],'/room/edit/{id}', 'RoomController@edit')->name('room.edit');
    Route::get('/room/delete/{id}', 'RoomController@delete')->name('room.delete');

    /* class */
    Route::get('/class', 'ClassController@index')->name('class');
    Route::match(['post', 'get'],'/class/add', 'ClassController@add')->name('class.add');
    Route::match(['post', 'get'],'/class/edit/{id}', 'ClassController@edit')->name('class.edit');
    Route::get('/class/delete/{id}', 'ClassController@delete')->name('class.delete');

    /* generate schedule */
    Route::post('/home/schedule', 'ScheduleController@get_schedule')->name('schedule');
    Route::match(['post', 'get'], '/schedule/generate/{key?}', 'ScheduleController@generate')->name('schedule.generate');
    Route::get('/schedule/detail/{id}', 'ScheduleController@detail')->name('schedule.detail');
    Route::post('/schedule/delete/{id}', 'ScheduleController@delete')->name('schedule.delete');
});

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');