let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    $(document).ready(function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
} else {
    console.error('CSRF token not found.');
}

function icheck() {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
}

function error_span(message) {
    var span = $('<span class="help-block"></span>');
    span.html(message);
    return span;
}

jQuery(document).ready(function ($) {
    //Date picker
    $('.datepicker, .input-group.date').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true,
        todayBtn: true
    });

    icheck();

    $(".disable-key").keydown(function (event) {
        return false;
    });

    $('.validate-error').change(function () {
        if ($(this).parent().hasClass('form-group')) {
            $(this).parent().removeClass('has-error');
            $(this).parent().find(".help-block").remove();
        } else if ($(this).parent().parent().hasClass('form-group')) {
            $(this).parent().parent().removeClass('has-error');
            $(this).parent().parent().find(".help-block").remove();
        }
    });
});