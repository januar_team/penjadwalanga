<?php

namespace App\Console\Commands;

use App\Lib\Genetic\Genetic;
use App\Model\Day;
use App\Model\Pengampu;
use App\Model\Room;
use App\Model\TimeSlot;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GeneticAlgorithm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ga:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to test run genetic algorithm by console';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = Day::get()->toArray();
        $time_slots = TimeSlot::get()->toArray();
        $rooms = Room::get()->toArray();
        $lecturer_course = Pengampu::join('courses','pengampus.course_id', '=', 'courses.id')
            ->whereRaw(DB::raw('MOD(courses.semester, 2)'), [1])
            ->with('lecturer')
            ->with('classes')
            ->with('course')
            ->select("pengampus.*")
            ->get()->toArray();

        $ga = new Genetic($days, $time_slots, $rooms, $lecturer_course);
        $ga->setTotalPopulation(10);
        $ga->setTotalGeneration(100);
        $ga->setMutationRate(0.05);
        $ga->run();
    }
}
