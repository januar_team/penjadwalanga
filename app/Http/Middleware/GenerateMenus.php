<?php

namespace App\Http\Middleware;

use Closure;
use Lavary\Menu\Menu;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if($user != null){
            \Menu::make('MyNavBar', function ($menu) {
                $menu->raw('MAIN NAVIGATION',['class' => 'header']);
                $menu->add('<span>Dashboard</span>', ['route' => 'dashboard'])
                    ->prepend('<i class="fa fa-dashboard"></i>');

                $menuTime = $menu->add('<span>Jadwal</span>', ['url' => '#','class' => 'treeview'])
                    ->append('<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>')
                    ->prepend('<i class="fa fa-calendar"></i>');
                $menuTime->add('<span>Hari</span>', ['route' => 'home.day'])
                    ->prepend('<i class="fa"></i>');
                $menuTime->add('Slot Waktu', ['route' => 'home.time'])
                    ->prepend('<i class="fa"></i>');
//                dd($menuTime);

                $menu->add('<span>Prody</span>', ['route' => 'prodi'])
                    ->prepend('<i class="fa fa-bookmark"></i>');

                $menu->add('<span>Dosen</span>', ['route' => 'lecturer'])
                    ->prepend('<i class="fa fa-user"></i>');
                $menu->add('Matakul', ['route' => 'course'])
                    ->prepend('<i class="fa fa-address-book"></i>');
                $menu->add('Kelas', ['route' => 'class'])
                    ->prepend('<i class="fa fa-users"></i>');
                $menu->add('Ruangan', ['route' => 'room'])
                    ->prepend('<i class="fa fa-building"></i>');
                $menu->add('Jadwal', ['route' => 'schedule.generate'])
                    ->prepend('<i class="fa fa-calendar"></i>');
            });
        }
        return $next($request);
    }
}
