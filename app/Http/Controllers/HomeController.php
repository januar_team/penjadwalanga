<?php

namespace App\Http\Controllers;

use App\Model\ClassRoom;
use App\Model\Course;
use App\Model\Day;
use App\Model\Lecturer;
use App\Model\Room;
use App\Model\TimeSlot;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->view([
            'room' => Room::count(),
            'class' => ClassRoom::count(),
            'lecturer' => Lecturer::count(),
            'course' => Course::count(),
        ]);
    }

    public function day(Request $request){
        return $this->view(['days' => Day::get()]);
    }

    public function time(Request $request){
        return $this->view(['times' => TimeSlot::get()]);
    }
}
