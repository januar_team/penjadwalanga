<?php

namespace App\Http\Controllers;

use App\Lib\Genetic\Chromosome;
use App\Lib\Genetic\Genetic;
use App\Model\Day;
use App\Model\Pengampu;
use App\Model\Room;
use App\Model\Schedule;
use App\Model\TimeSlot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ScheduleController extends Controller
{
    public function generate(Request $request, $hash = null){
        set_time_limit(0);
        if ($request->isMethod('post')){
            if ($hash && $request->session()->has($hash)){
                $data = $request->session()->get($hash);
//                $logLines = file($data['logs']);
//                $logs = "";
//                foreach($logLines as $line){
//                    $logs .= "<p>".$line."</p>";
//                }

                $schedule = new Schedule();
                $schedule->tahun_ajaran = $data['tahun_ajaran'];
                $schedule->semester = $data['semester'];
                $schedule->population = $data['population'];
                $schedule->generation = $data['generation'];
                $schedule->mutation_rate = $data['mutation_rate'];
                $schedule->iteration = $data['iteration'];
                $schedule->logs = $data['logs'];
                $schedule->result = json_encode($data['result']);
                $schedule->save();

                return redirect()->route('schedule.generate');
            }

            $this->validate($request, [
                'tahun_ajaran' => 'required',
                'semester' => 'required',
                'population' => 'required',
                'generation' => 'required',
                'mutation_rate' => 'required'
            ]);

            $executionStartTime = microtime(true);
            $days = Day::get()->toArray();
            $time_slots = TimeSlot::get()->toArray();
            $rooms = Room::get()->toArray();
            $lecturer_course = Pengampu::join('courses','pengampus.course_id', '=', 'courses.id')
                ->whereRaw(DB::raw('MOD(courses.semester, 2)'), [$request->semester])
                ->with('lecturer')
                ->with('classes')
                ->with('course')
                ->select("pengampus.*")
                ->get()->toArray();

            $ga = new Genetic($days, $time_slots, $rooms, $lecturer_course);
            $ga->setTotalPopulation($request->population);
            $ga->setTotalGeneration($request->generation);
            $ga->setMutationRate($request->mutation_rate);
            $iteration = $ga->run();
            $result = $ga->getResult();
            $executionEndTime = microtime(true);

            $key = str_random();
            session([$key => [
                'tahun_ajaran' => $request->tahun_ajaran,
                'semester' => $request->semester,
                'population' => $request->population,
                'generation' => $request->generation,
                'mutation_rate' => $request->mutation_rate,
                'iteration' => $iteration,
                'logs' => $ga->log_filename,
                'result' => $result,
                'exec_time' => $executionEndTime - $executionStartTime
            ]]);

            return response()->json(['key' => $key]);
        }

        if ($hash && $request->session()->has($hash)){
            $data = $request->session()->get($hash);
            $days = Day::get();
            $time_slots = TimeSlot::get();
            $data = array_merge($data , [
                'key' => $hash,
                'days' => $days,
                'times' => $time_slots
            ]);

            return $this->view($data);
        }
        return $this->view();
    }

    public function get_schedule(Request $request){
        return DataTables::of(Schedule::query())->make(true);
    }

    public function detail(Request $request, $id){
        $schedule = Schedule::find($id);
        if (!isset($schedule)){
            return redirect()->route('dashboard');
        }

        $obj = json_decode($schedule->result, true);
        $result = new Chromosome($obj['gen']);
        $result->fitness = $obj['fitness'];

        $days = Day::get();
        $time_slots = TimeSlot::get();

        return $this->view([
            'schedule' => $schedule,
            'result' => $result,
            'days' => $days,
            'times' => $time_slots
        ]);
    }

    public function delete(Request $request, $id){
        $schedule = Schedule::find($id);
        if($schedule){
            $schedule->delete();
        }

        return response()->json(['status' => true]);
    }
}
