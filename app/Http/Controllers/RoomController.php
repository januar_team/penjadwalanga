<?php

namespace App\Http\Controllers;

use App\Model\Room;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    public function index(Request $request){
        $rooms = Room::orderBy('name')->paginate(10);
        return $this->view(['rooms' => $rooms, 'page' => ($request->page)?:1]);
    }

    public function add(Request $request){
        if ($request->isMethod('post')){
            $this->validate($request, [
                'name' => 'required|unique:rooms,name',
                'floor' => 'required',
            ]);

            $room = new Room($request->all());
            $room->save();

            return redirect(route('room'));
        }
        return $this->view();
    }

    public function edit(Request $request, $id){
        $room = Room::find($id);
        if (!$room)
            return redirect(route('room'));

        if ($request->isMethod('post')){
            $validation = [
                'name' => 'required',
                'floor' => 'required',
            ];

            if ($room->name != $request->name)
                $validation['name'] .= '|unique:rooms,name';

            $this->validate($request, $validation);

            $room->fill($request->all());
            $room->save();

            return redirect(route('room') . "?page=$request->page");
        }
        return $this->view(['room' => $room, 'page' => $request->page]);
    }

    public function delete(Request $request, $id){
        $room = Room::find($id);
        if($room){
            $room->delete();
        }

        return redirect(route('room'));
    }
}
