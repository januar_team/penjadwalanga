<?php

namespace App\Http\Controllers;

use App\Model\ClassRoom;
use App\Model\Course;
use App\Model\Lecturer;
use App\Model\Pengampu;
use App\Model\Prodi;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index(Request $request){
        $prodi = $request->prodi;
        $courses = Course::orderBy('semester');
        if ($prodi)
            $courses->where('prodi_id', $prodi);

        $courses = $courses->paginate(10);
        $prodis = Prodi::all();
        return $this->view(['courses' => $courses, 'prodis' => $prodis, 'prodi' => $prodi, 'page' => ($request->page)?:1]);
    }

    public function add(Request $request){
        if ($request->isMethod('post')){
            $this->validate($request, [
                'code' => 'required|unique:courses,code',
                'name' => 'required',
                'semester' => 'required|in:1,2,3,4,5,6,7,8',
                'jumlah_sks' => 'required|integer|min:1',
                'prodi_id' => 'required|exists:prodis,id'
            ]);

            $course = new Course($request->all());
            $course->save();

            return redirect(route('course'));
        }
        $prodi = Prodi::all();
        return $this->view(['prodi' => $prodi]);
    }

    public function edit(Request $request, $id){
        $course = Course::find($id);
        if (!$course)
            return redirect(route('course'));

        if ($request->isMethod('post')){
            $validation = [
                'code' => 'required',
                'name' => 'required',
                'semester' => 'required|in:1,2,3,4,5,6,7,8',
                'jumlah_sks' => 'required|integer|min:1',
                'prodi_id' => 'required|exists:prodis,id'
            ];

            if ($course->code != $request->code)
                $validation['code'] .= '|unique:courses,code';

            $this->validate($request, $validation);

            $course->fill($request->all());
            $course->save();

            return redirect(route('course') . "?page=$request->page");
        }
        $prodi = Prodi::all();
        return $this->view(['course' => $course, 'page' => $request->page, 'prodi' => $prodi]);
    }

    public function pengampu(Request $request, $id){
        $course = Course::find($id);
        if (!$course)
            return redirect(route('course'));

        $pengampu = Pengampu::where('course_id', $id)->get();

        if ($request->isMethod('post')){
            $this->validate($request ,[
                'lecturer_id' => 'required',
                'course_id' => 'required',
                'class_id' => 'required',
            ]);

            try{
                $pengampu = ($request->id)? Pengampu::find($request->id): new Pengampu();
                $pengampu->fill($request->all());
                $pengampu->save();
            }catch (\Exception $e){
                if ($e->getCode() == 23000){
                    return response()->json(['status' => false, 'message' => 'Data sudah tersedia'], 400);
                }else{
                    throw $e;
                }
            }
            return response()->json(['status' => true]);
        }
        return $this->view(
            [
                'course' => $course,
                'pengampu' => $pengampu,
                'lecturer' => Lecturer::get(),
                'class'   => ClassRoom::get(),
                'page' => $request->page
            ]
        );
    }

    public function delete(Request $request, $id){
        $course = Course::find($id);
        if($course){
            $course->delete();
        }

        return redirect(route('course'));
    }

    public function pengampu_delete(Request $request, $course, $id){
        $pengampu = Pengampu::find($id);
        if($pengampu){
            $pengampu->delete();
        }

        return redirect(route('course.pengampu', ['id' => $course, 'prodi' => $request->prodi, 'page' => $request->page]));
    }
}
