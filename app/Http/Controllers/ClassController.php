<?php

namespace App\Http\Controllers;

use App\Model\ClassRoom;
use Illuminate\Http\Request;

class ClassController extends Controller
{
    public function index(Request $request){
        $classes = ClassRoom::orderBy('type')->paginate(10);
        return $this->view(['classes' => $classes, 'page' => ($request->page)?:1]);
    }

    public function add(Request $request){
        if ($request->isMethod('post')){
            $this->validate($request, [
                'code' => 'required|unique:classes,code',
                'type' => 'required|in:pagi,sore',
            ]);

            $class = new ClassRoom($request->all());
            $class->save();

            return redirect(route('class'));
        }
        return $this->view();
    }

    public function edit(Request $request, $id){
        $class = ClassRoom::find($id);
        if (!$class)
            return redirect(route('class'));

        if ($request->isMethod('post')){
            $validation = [
                'code' => 'required',
                'type' => 'required|in:pagi,sore',
            ];

            if ($class->code != $request->code)
                $validation['code'] .= 'unique:classes,code';

            $this->validate($request, $validation);

            $class->fill($request->all());
            $class->save();

            return redirect(route('class') . "?page=$request->page");
        }
        return $this->view(['class' => $class, 'page' => $request->page]);
    }

    public function delete(Request $request, $id){
        $class = ClassRoom::find($id);
        if($class){
            $class->delete();
        }

        return redirect(route('class'));
    }
}
