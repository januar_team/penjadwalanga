<?php

namespace App\Http\Controllers;

use App\Model\Prodi;
use Illuminate\Http\Request;

class ProdiController extends Controller
{
    public function index(Request $request){
        $prodi = Prodi::orderBy('name')->get();
        return $this->view(['prodis' => $prodi]);
    }

    public function add(Request $request){
        if ($request->isMethod('post')){
            $this->validate($request, [
                'name' => 'required'
            ]);

            $prodi = new Prodi();
            $prodi->name = $request->name;
            $prodi->description = $request->description;
            $prodi->save();

            return redirect(route('prodi'));
        }
        return $this->view();
    }

    public function edit(Request $request, $id){
        $prodi = Prodi::find($id);
        if (!$prodi)
            return redirect(route('prodi'));

        if ($request->isMethod('post')){
            $this->validate($request, [
                'name' => 'required'
            ]);

            $prodi->name = $request->name;
            $prodi->description = $request->description;
            $prodi->save();

            return redirect(route('prodi'));
        }
        return $this->view(['prodi' => $prodi]);
    }

    public function delete(Request $request, $id){
        $prodi = Prodi::find($id);
        if($prodi){
            $prodi->delete();
        }

        return redirect(route('prodi'));
    }
}
