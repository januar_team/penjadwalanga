<?php

namespace App\Http\Controllers;

use App\Model\Lecturer;
use Illuminate\Http\Request;

class LecturerController extends Controller
{
    public function index(Request $request){
        $lecturer = Lecturer::orderBy('name')->get();
        return $this->view(['lecturer' => $lecturer]);
    }

    public function add(Request $request){
        if ($request->isMethod('post')){
            $this->validate($request, [
                'nidn' => 'required|unique:lecturers,nidn',
                'name' => 'required'
            ]);

            $lecturer = new Lecturer();
            $lecturer->nidn = $request->nidn;
            $lecturer->name = $request->name;
            $lecturer->save();

            return redirect(route('lecturer'));
        }
        return $this->view();
    }

    public function edit(Request $request, $id){
        $lecturer = Lecturer::find($id);
        if (!$lecturer)
            return redirect(route('lecturer'));

        if ($request->isMethod('post')){
            $validation = [
                'nidn' => 'required',
                'name' => 'required'
            ];

            if ($lecturer->nidn != $request->nidn)
                $validation['nidn'] .= '|unique:lecturers,nidn';

            $this->validate($request, $validation);

            $lecturer->nidn = $request->nidn;
            $lecturer->name = $request->name;
            $lecturer->save();

            return redirect(route('lecturer'));
        }
        return $this->view(['lecturer' => $lecturer]);
    }

    public function delete(Request $request, $id){
        $lecturer = Lecturer::find($id);
        if($lecturer){
            $lecturer->delete();
        }

        return redirect(route('lecturer'));
    }
}
