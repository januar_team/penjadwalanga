<?php
/**
 * Created by PhpStorm.
 * User: januar
 * Date: 5/23/18
 * Time: 5:29 PM
 */

namespace App\Lib;


use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\StreamHandler;

trait Logger
{
    /**
     * Monolog Logger Object
     *
     * @var \Monolog\Logger
    */
    static $log;

    public $log_filename;

    /**
     * initialization logger object
     *
     * @param string $name
     * @return void
    */
    protected function initLogger($name = "logger"){
        self::$log = new \Monolog\Logger($name);

        // the default date format is "Y-m-d H:i:s"
        $dateFormat = "Y-m-d H:i:s";
        // the default output format is "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n"
        $output = "%datetime% %level_name% : %message%\n";
        // finally, create a formatter
        $formatter = new LineFormatter($output, $dateFormat);

        $date = Carbon::now()->format('Y-m-d H-i-s');
        $this->log_filename = storage_path("logs/$name/$date.log");
        $stream = new StreamHandler($this->log_filename);
        $stream->setFormatter($formatter);

        self::$log->pushHandler($stream);
        self::$log->pushHandler(new FirePHPHandler());
    }

    public static function error($message){
        self::$log->error($message);
    }

    public static function info($message){
        self::$log->info($message);
    }

    public static function label($label){
        self::line();

        $count = strlen($label);
        $pad = floor($count / 2);
        $format = "%' " . (25 - $pad - 1) . "s%s%' " . (25 - $pad);
        self::info(sprintf($format, " ", $label, ""));
        self::line();
    }

    public static function line(){
        self::info(sprintf("%'-50s", ""));
    }

    public static function console($message){
        if (App::runningInConsole()){
            echo($message ."\n");
        }
    }
}