<?php
/**
 * Created by PhpStorm.
 * User: Januar Sirait <januar.srt@gmail.com>
 * Date: 5/23/18
 * Time: 11:45 AM
 */

namespace App\Lib\Genetic;

use App\Lib\Genetic\Exception\DataNotValidException;
use App\Lib\Genetic\Exception\GenerationInvalidException;
use App\Lib\Logger;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Psy\Input\CodeArgument;

class Genetic
{
    use Logger;

    /**
     * Default total population will generate
     *
     * @var integer
    */
    const DEFAULT_TOTAL_POPULATION = 20;

    /**
     * Total of population wil generate
     *
     * @var integer
     */
    private $total_population;

    /**
     * Length og chromosome (total gen in a chromosome)
     *
     * @var integer
    */
    private $gen_length;

    /**
     * Total generation will run
     *
     * @var integer
    */
    private $total_generation;

    /**
     * State active generation
     *
     * @var integer
     */
    private $generation;

    /**
     * Array of available days
     *
     * @var array
    */
    private $days;

    /**
     * Array of time slot
     *
     * @var array
    */
    private $time_slots;

    /**
     * Array of available rooms
     *
     * @var array
    */
    private $rooms;

    /**
     * Array of available course, lecturer and class
     *
     * @var array
    */
    private $lecturer_course;

    /**
     * Array of population will process
     *
     * @var array
    */
    private $population;

    /**
     * Integer of mutation rate
     *
     * @var integer
     */
    private $mutation_rate;

    private $matrix_ruangan;

    /**
     * Create a new Genetic instance.
     *
     * @param array  $days
     * @param array $time_slots
     * @param array $rooms
     * @param array $lecturer_course
     * @return void
     */
    public function __construct(array $days, array $time_slots, array $rooms, array $lecturer_course)
    {
        $this->total_population = self::DEFAULT_TOTAL_POPULATION;
        $this->setDays($days);
        $this->setTimeSlots($time_slots);
        $this->setRooms($rooms);
        $this->setLecturerCourse($lecturer_course);

        $this->gen_length = count($days) * count($time_slots) * count($rooms);

        $this->initLogger("ga");
    }

    /**
     * Setter $total_population
     *
     * @param $population
     * @return void
    */
    public function setTotalPopulation($population){
        $this->total_population = $population;
    }

    /**
     * Setter $total_generation
     *
     * @param $total_generation
     * @return void
     */
    public function setTotalGeneration($total_generation){
        $this->total_generation = $total_generation;
    }

    /**
     * Setter $this->days
     *
     * @param array $days
     * @return void
    */
    public function setDays(array $days){
        $this->days = $days;
    }

    /**
     * Setter $this->time_slots
     *
     * @param array $time_slots
     * @return void
     */
    public function setTimeSlots(array $time_slots){
        $this->time_slots = $time_slots;
    }

    /**
     * Setter $this->rooms
     *
     * @param array $rooms
     * @return void
     */
    public function setRooms(array $rooms){
        $this->rooms = $rooms;
    }

    /**
     * Setter $this->lecturer_course
     *
     * @param array $lecturer_course
     * @return void
     */
    public function setLecturerCourse(array $lecturer_course){
        $this->lecturer_course = $lecturer_course;
    }

    /**
     * Setter $this->>mutation_rate
     *
     * @param integer $mutation_rate
     * @return void
    */
    public function setMutationRate($mutation_rate){
        $this->mutation_rate = $mutation_rate;
    }

    /**
     * Generate the initial population for GA
    */
    public function initPopulation(){
        if (!count($this->days))
            throw (new DataNotValidException())->setData("days")->handle($this);
        self::info("Total days : " . count($this->days));

        if (!count($this->time_slots))
            throw (new DataNotValidException())->setData("time_slots")->handle($this);
        self::info("Total time_slots : " . count($this->time_slots));

        if (!count($this->rooms))
            throw (new DataNotValidException())->setData("rooms")->handle($this);
        self::info("Total rooms : " . count($this->rooms));

        if (!count($this->lecturer_course))
            throw (new DataNotValidException())->setData("lecturer_course")->handle($this);
        self::info("Total lecturer_course : " . count($this->lecturer_course));

        self::console("Generate population");
        /**
         * membuat matrix ruangan, hari dan waktu
        */
        for ($i = 0 ; $i < count($this->rooms); $i++){
            for ($j = 0 ; $j < count($this->days); $j++){
                for ($k = 0; $k < count($this->time_slots); $k++){
                    $this->matrix_ruangan[] = [
                        'room' => $this->rooms[$i],
                        'day' => $this->days[$j],
                        'time' => $this->time_slots[$k],
                    ];
                }
            }
        }

        for ($i = 0; $i < $this->total_population; $i++){
            $gen = $this->matrix_ruangan;
            for ($j = 0; $j < count($this->lecturer_course); $j ++){
                do {
                    $position = rand(0, count($gen) - 2);
                }while (!$this->is_valid_position($position, $gen, $this->lecturer_course[$j]['course']['jumlah_sks']));

                for ($k = 0; $k < $this->lecturer_course[$j]['course']['jumlah_sks']; $k++){
                    $gen[$position + $k]['course'] = $this->lecturer_course[$j];
                    $gen[$position + $k]['sks'] = $k + 1;
                }
            }

            $chromosome = new Chromosome($gen);
            $chromosome->days = count($this->days);
            $this->population[] = $chromosome;
        }
//        Storage::put('ga/population.json', json_encode($this->population, JSON_PRETTY_PRINT));

//        $temp = json_decode(Storage::get('ga/population.json'), true);
//        for ($i = 0; $i < count($temp); $i++){
//            $chromosome = new Chromosome($temp[$i]['gen']);
//            $chromosome->days = count($this->days);
//            $this->population[] = $chromosome;
//        }
    }

    private function is_valid_position($position, $gen, $sks){
        if (isset($gen[$position]['course']))
            return false;

        for ($i = 1; $i < $sks; $i++){
            if (isset($gen[$position + $i]['course']))
                return false;

            if (!isset($gen[$position + $i]))
                return false;

            if ($gen[$position]['day']['id'] != $gen[$position + $i]['day']['id'])
                return false;

            if ($gen[$position]['room']['id'] != $gen[$position + $i]['room']['id'])
                return false;
        }

        return true;
    }

    public function count_fitness(){
        self::console("count fitness");
        for ($i = 0; $i < count($this->population); $i++){
            self::info(sprintf("count fitness chromosome %d", $i));
            $this->population[$i]->fitness();
        }
    }

    public function count_fitness_relative(){
        self::console("count fitness relative");
        $total_fittnes = 0;
        array_walk($this->population, function($item) use(&$total_fittnes){
            $total_fittnes += $item->fitness;
        });

        for ($i = 0; $i < count($this->population); $i++){
            $this->population[$i]->fitness_relative = (float)$this->population[$i]->fitness / (float)$total_fittnes;
            self::info(sprintf("fitness relative chromosome %d = %f, fitness = %f", $i,
                $this->population[$i]->fitness_relative, $this->population[$i]->fitness));
        }
    }

    /*
	* sort individu berdasarkan fitness. fitness terbaik berada pada urutan paling atas
	* ASC
	*/
    public function sort()
    {
        for ($i=0; $i < count($this->population) - 1; $i++) {
            $bigger = $this->population[$i]->fitness_relative;
            $index = $i;
            for ($j=$i; $j < count($this->population) ; $j++) {
                if ($this->population[$j]->fitness_relative > $bigger) {
                    $index = $j;
                    $bigger = $this->population[$j]->fitness_relative;
                }
            }

            //swap
            $temp = clone $this->population[$i];
            $this->population[$i] = clone $this->population[$index];
            $this->population[$index] = clone $temp;
        }
    }

    public function cross_over(){
        self::console("cross over");
        $total_gen = count($this->population[0]->gen) - 1;
        for ($i = 0; $i < $this->total_population; $i+=2 ){
            $idx_parent1 = rand(0, $this->total_population - 1);
            do{
                $idx_parent2 = rand(0, $this->total_population - 1);
            }while($idx_parent1 == $idx_parent2);

            $parent1 = $this->population[$idx_parent1];
            $parent2 = $this->population[$idx_parent2];

            do{
                $start = rand(0, $total_gen - 2);
                $is_valid = $this->validate_cross_start_point($start, $parent1, $parent2);
//                self::console(sprintf('position : %d , %s', $start, $is_valid ? 'true' : 'false'));
            }while(!$is_valid);

            do {
                $end = rand($start, $total_gen);
                $is_valid = $this->validate_cross_end_point($end, $parent1, $parent2);
//                self::console(sprintf('position : %d , %s', $end, $is_valid ? 'true' : 'false'));
            }while (!$is_valid);
            $gen = [];

            for ($j = 0 ; $j <= $total_gen; $j++){
                if ($j >= $start && $j <= $end){
                    $gen[] = $parent2->gen[$j];
                }else{
                    $gen[] = $parent1->gen[$j];
                }
            }
            $child = new Chromosome($gen);
            $child->days = count($this->days);
            $this->population[] = $child;
        }
    }

    private function validate_cross_start_point(&$position, Chromosome $parent1, Chromosome $parent2){
//        self::console(sprintf("\nStart %d, isset parent1 %s ", $position,
//            isset($parent1->gen[$position]['course']) ? 'true': 'false'));
        if (isset($parent1->gen[$position]['course'])){
//            self::console(sprintf("sks %d", $parent1->gen[$position]['sks']));
            $position -= $parent1->gen[$position]['sks'] - 1;
        }

        if ($position < 0 || $position == count($parent1->gen)) return false;

//        self::console(sprintf("Start %d, isset parent2 %s ", $position,
//            isset($parent2->gen[$position]['course']) ? 'true': 'false'));
        if (isset($parent2->gen[$position]['course'])){
//            self::console(sprintf("sks %d", $parent2->gen[$position]['sks']));
            if ($parent2->gen[$position]['sks'] != 1) {
                return false;
            }
        }

        return true;
    }

    private function validate_cross_end_point(&$position, Chromosome $parent1, Chromosome $parent2){
//        self::console(sprintf("\nEnd %d, isset parent1 %s", $position,
//            isset($parent1->gen[$position]['course']) ? 'true': 'false'));
        if (isset($parent1->gen[$position]['course'])){
//            self::console(sprintf("sks %d", $parent1->gen[$position]['sks']));
            $position += $parent1->gen[$position]['course']['course']['jumlah_sks'] - $parent1->gen[$position]['sks'];
        }

        if ($position >= count($parent1->gen)) return false;

//        self::console(sprintf("End %d, isset parent2 %s", $position,
//            isset($parent2->gen[$position]['course']) ? 'true': 'false'));
        if (isset($parent2->gen[$position]['course'])){
//            self::console(sprintf("sks %d", $parent2->gen[$position]['sks']));
            if ($parent2->gen[$position]['course']['course']['jumlah_sks'] != $parent2->gen[$position]['sks']) {
                return false;
            }
        }

        return true;
    }

    private function mutation(){
        self::console("mutation");
        $total_gen = count($this->population) * $this->gen_length;
        $total_gen_mutation = (int)($this->mutation_rate * $total_gen);

        Genetic::line();
        Genetic::info(sprintf("START MUTATION, total gen: %d, 
        mutation rate : %f", $total_gen_mutation, $this->mutation_rate));

        for ($i=0 ; $i < $total_gen_mutation; $i++){
            Genetic::info(sprintf("Mutation : %d", $i));

            $chromosome_position = rand(0, count($this->population) - 1);
            $gen_position = rand(0, $this->gen_length - 2);

            Genetic::info(sprintf("Mutation %d, chromosome : %d, gen : %d",
                $i, $chromosome_position, $gen_position));

            /**
             * start swap and checking gen
            */
            if (isset($this->population[$chromosome_position]->gen[$gen_position]['course'])){
                $temp_sks = $this->population[$chromosome_position]->gen[$gen_position]['sks'];
                $gen_position -= $this->population[$chromosome_position]->gen[$gen_position]['sks'] - 1;
                Genetic::info(sprintf("sks position: %d, update position: %d", $temp_sks, $gen_position));
            }else{
                Genetic::info("Nothing need to swap..");
                continue;
            }

            if (!isset($this->population[$chromosome_position]->gen[$gen_position]['course'])){
                echo "error";
            }

            // get sks of course
            $sks = $this->population[$chromosome_position]->gen[$gen_position]['course']['course']['jumlah_sks'];
            /* end get first position */

            /* start get second position */
            do{
                $swap_position = rand(0, $this->gen_length - 2);
            }while(!$this->validate_second_position_mutation($swap_position, $gen_position, $chromosome_position));

            Genetic::info(sprintf("Swap position %d", $swap_position));
            // call swap function
            $this->swap_mutation_gen($chromosome_position, $gen_position, $swap_position, $sks);
            Genetic::line();
        }
    }

    private function validate_second_position_mutation($position, $first_position, $chromosome){
        if ($position == $first_position)
            return false;

        $sks = $this->population[$chromosome]->gen[$first_position]['course']['course']['jumlah_sks'];

        if (isset($this->population[$chromosome]->gen[$position]['course'])){
            if ($this->population[$chromosome]->gen[$position]['sks'] != 1){
                return false;
            }
        }

        if (isset($this->population[$chromosome]->gen[$position + $sks - 1]['course'])){
            if ($this->population[$chromosome]->gen[$position + $sks - 1]['sks'] !=
                $this->population[$chromosome]->gen[$position + $sks - 1]['course']['course']['jumlah_sks']){
               return false;
            }
        }

        if ($position + $sks >= $this->gen_length){
            return false;
        }

        for ($i = $position; $i < ($position + $sks); $i++){
            if ($this->population[$chromosome]->gen[$i]['day']['id'] !=
                $this->population[$chromosome]->gen[$position]['day']['id']){
                return false;
            }
        }

        for ($i = $position; $i < ($position + $sks); $i++){
            if ($this->population[$chromosome]->gen[$i]['room']['id'] !=
                $this->population[$chromosome]->gen[$position]['room']['id']){
                return false;
            }
        }

        return true;
    }

    /**
     * swap mutation gen
     *
     * @param $chromosome_position int
     * @param $gen_position int
     * @param $gen_swap_position int
     * @param $sks int
     * @return void
    */
    private function swap_mutation_gen($chromosome_position, $gen_position, $gen_swap_position,
                                       $sks){
        Genetic::info("Start Swap");

        /*if (isset($this->population[$chromosome_position]->gen[$gen_position]['course'])){
            if ($this->population[$chromosome_position]->gen[$gen_position]['sks'] != 1){
                self::console("swap1");
                dd([$chromosome_position, $gen_position, $gen_swap_position,
                    $sks]);
            }
        }

        if (isset($this->population[$chromosome_position]->gen[$gen_swap_position + $sks - 1]['course'])){
            if ($this->population[$chromosome_position]->gen[$gen_swap_position + $sks - 1]['course']['course']['jumlah_sks'] !=
                $this->population[$chromosome_position]->gen[$gen_swap_position + $sks - 1]['sks']){
                self::console("swap2");
                dd([$chromosome_position, $gen_position, $gen_swap_position,
                    $sks]);
            }
        }*/

        $temp = [];
        for ($i = $gen_position; $i < $gen_position + $sks; $i++){
            $temp[] = $this->population[$chromosome_position]->gen[$gen_position]['course'];
        }

        for ($i = 0; $i < $sks; $i++){
            if (isset($this->population[$chromosome_position]->gen[$gen_swap_position + $i]['course'])) {
                $this->population[$chromosome_position]->gen[$gen_position + $i]['course'] =
                    $this->population[$chromosome_position]->gen[$gen_swap_position + $i]['course'];
                $this->population[$chromosome_position]->gen[$gen_position + $i]['sks'] =
                    $this->population[$chromosome_position]->gen[$gen_swap_position + $i]['sks'];
            }else{
                unset($this->population[$chromosome_position]->gen[$gen_position + $i]['course']);
                unset($this->population[$chromosome_position]->gen[$gen_position + $i]['sks']);
            }
        }

        for ($i = 0; $i < $sks; $i++){
            $this->population[$chromosome_position]->gen[$gen_swap_position + $i] = array_merge(
                $this->population[$chromosome_position]->gen[$gen_swap_position + $i], [
                    'course' => $temp[$i],
                    'sks' => ($i) + 1
                ]
            );
            sleep(0);
        }

        Genetic::info("End Swap");
    }

    private function selection(){
        self::console("selection \n");
        self::label("START SELECTION");
        self::info("SORTING ALL CHROMOSOME");
        $new_population = [];
        $this->sort();
        for ($i = 0; $i < count($this->population); $i++){
            self::info(sprintf("fitness relative chromosome %d = %f, fitness = %f", $i,
                $this->population[$i]->fitness_relative, $this->population[$i]->fitness));

            if ($i < $this->total_population){
                $new_population[] = clone $this->population[$i];
            }
        }

        self::line();
        self::info("NEW POPULATION");
        $this->population = $new_population;
        for ($i = 0; $i < count($this->population); $i++){
            self::info(sprintf("fitness relative chromosome %d = %f, fitness = %f", $i,
                $this->population[$i]->fitness_relative, $this->population[$i]->fitness));
        }
    }

    public function getResult(){
        if (count($this->population) <= 0){
            return null;
        }

        return $this->population[0];
    }

    /**
     * Start the genetic algorithm
     *
     * @return integer
    */
    public function run(){
        if ($this->total_generation <= 0) {
            throw (new GenerationInvalidException())->handle($this);
        }
        if ($this->mutation_rate <= 0 )
            throw (new DataNotValidException("Mutation rate can not set as zero or negative number."))->handle($this);
        self::info("Mutation rate : " . $this->mutation_rate);

        $this->initPopulation();
        self::label("START");
        self::info("GENERATION : 0");
        $this->count_fitness();
        $this->count_fitness_relative();
        $this->selection();

        self::console(sprintf("GENERATION %d, fitness : %f", 0, $this->population[0]->fitness));

        $generation = 1;
        while ($generation < $this->total_generation && $this->population[0]->fitness < 1){
            self::label(sprintf("GENERASI KE-%d ", $generation));
            $this->cross_over();
            $this->mutation();
            $this->count_fitness();
            $this->count_fitness_relative();
            $this->selection();
            self::console(sprintf("\nGENERATION %d, fitness : %f", $generation, $this->population[0]->fitness));
            $generation++;
        }
        Storage::put('ga/log/result.json', json_encode($this->population, JSON_PRETTY_PRINT));
        self::label("END");

        return $generation;
    }

    private function isValid(){
        for ($i = 0; $i < count($this->population); $i++){
            $gen = array_filter($this->population[$i]->gen, function ($item){
               return(isset($item['course']));
            });

            foreach ($gen as $item){
                if ($item['sks'] == 1){
                    $temp = $item;
                    continue;
                }

                if (!isset($temp)){
                    self::console("invalid data 1");
                    dd($item);
                }

                if ($item['room']['id'] != $temp['room']['id']){
                    self::console("invalid data 2");
                    dd($item, $temp);
                }

                if ($item['day']['id'] != $temp['day']['id']){
                    self::console("invalid data 3");
                    dd($item, $temp);
                }

                if ($item['sks'] == $item['course']['course']['jumlah_sks'])
                    $temp = null;
            }
        }
        self::console("valid data");
    }
}