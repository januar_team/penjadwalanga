<?php
/**
 * Created by PhpStorm.
 * User: januar
 * Date: 5/23/18
 * Time: 4:08 PM
 */

namespace App\Lib\Genetic\Exception;


use Throwable;

class GenerationInvalidException extends GeneticException
{
    public function __construct()
    {
        parent::__construct("Max generation that will generated invalid", 0, null);
    }
}