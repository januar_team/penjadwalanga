<?php
/**
 * Created by PhpStorm.
 * User: januar
 * Date: 5/23/18
 * Time: 5:57 PM
 */

namespace App\Lib\Genetic\Exception;

class GeneticException extends \RuntimeException
{
    /**
     * To print log to file
     *
     * @param \App\Lib\Logger $logger
     * @return GeneticException
    */
    public function handle($logger){
        $logger->error($this->message);
        return $this;
    }
}