<?php
/**
 * Created by PhpStorm.
 * User: januar
 * Date: 5/23/18
 * Time: 3:57 PM
 */

namespace App\Lib\Genetic\Exception;


class DataNotValidException extends GeneticException
{
    public function setData($data){
        $this->message = "No found data or empty for [{$data}]";
        return $this;
    }
}