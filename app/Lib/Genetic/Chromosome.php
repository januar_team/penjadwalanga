<?php
/**
 * Created by PhpStorm.
 * User: Januar
 * Date: 6/2/2018
 * Time: 12:17 AM
 */

namespace App\Lib\Genetic;


use Carbon\Carbon;

class Chromosome
{
    const PENALTY_LECTURER = "lecturer_clash";
    const PENALTY_CLASS = "class_clash";
    const PENALTY_MORNING_CLASS = "morning_class";
    const PENALTY_AFTERNOON_CLASS = "afternoon_class";

    const CLASS_TYPE_MORNING = "pagi";
    const CLASS_TYPE_AFTERNOON = "sore";

    /**
     * List of gen
     *
     * @var array
    */
    public $gen;

    /**
     * List of penalty
     *
     * @var array
    */
    public $penalty;

    /**
     * Total penalty
     *
     * @var int
    */
    public $total_penalty;

    public $fitness;

    public $fitness_relative;

    public $days;

    /**
     * @var Carbon mid time
    */
    private $midTime;

    public function __construct($gen)
    {
        $this->gen = $gen;
        $this->midTime = Carbon::createFromTimeString('13:00:00');
        $this->penalty = [
            self::PENALTY_LECTURER => 0,
            self::PENALTY_CLASS => 0,
            self::PENALTY_MORNING_CLASS => 0,
            self::PENALTY_AFTERNOON_CLASS => 0,
        ];
    }

    public function getGen(){
        return $this->gen;
    }

    public function count_penalty(){
        $this->penalty_lecturer();
        $this->penalty_class();
        $this->penalty_morning_class();
        $this->penalty_afternoon_class();
        $this->total_penalty = array_sum(array_values($this->penalty));
        Genetic::info("TOTAL PENALTY : " . $this->total_penalty);
    }

    public function penalty_lecturer(){
        Genetic::label("COUNT LECTURER PENALTY");
        $this->penalty[self::PENALTY_LECTURER] = 0;
        for ($i = 1; $i <= $this->days; $i++){
            $slot = array_filter($this->gen, function ($item) use ($i){
                return ($item['day']['id'] == $i) && isset($item['course']);
            });

            $temp = [];
            foreach ($slot as $key => $lec){
                if (in_array($key, $temp))
                    continue;

                $found = array_filter($slot, function ($item) use ($lec){
                    return(
                        ($item['course']['id'] != $lec['course']['id']) &&
                        ($item['course']['lecturer_id'] == $lec['course']['lecturer_id']) &&
                        ($item['time']['id'] == $lec['time']['id'])
                    );
                });

                if (count($found) > 0) {
                    $this->penalty[self::PENALTY_LECTURER]++;
                    $temp = array_merge($temp, [$key], array_keys($found));

                    /**
                     * @todo print log
                    */
                    Genetic::info(sprintf("Current : %s, class : %s, room : %s, time : %s",
                        $lec['course']['lecturer']['name'], $lec['course']['classes']['code'],
                        $lec['room']['name'], $lec['time']['start']));

                    foreach ($found as $item){
                        Genetic::info(sprintf("Checked : %s, class : %s, room : %s, time : %s",
                            $item['course']['lecturer']['name'], $item['course']['classes']['code'],
                            $item['room']['name'], $item['time']['start']));
                    }
                    /* @todo end print log */
                }
            }
        }

        Genetic::info(sprintf("penalty : %d", $this->penalty[self::PENALTY_LECTURER]));
    }

    public function penalty_class(){
        Genetic::label("COUNT CLASS PENALTY");
        $this->penalty[self::PENALTY_CLASS] = 0;
        for ($i = 1; $i <= $this->days; $i++){
            $slot = array_filter($this->gen, function ($item) use ($i){
                return ($item['day']['id'] == $i) && isset($item['course']);
            });

            $temp = [];
            foreach ($slot as $key => $lec){
                if (in_array($key, $temp))
                    continue;

                $found = array_filter($slot, function ($item) use ($lec){
                    return(
                        ($item['course']['id'] != $lec['course']['id']) &&
                        ($item['course']['class_id'] == $lec['course']['class_id']) &&
                        ($item['time']['id'] == $lec['time']['id'])
                    );
                });

                if (count($found) > 0) {
                    $this->penalty[self::PENALTY_CLASS]++;
                    $temp = array_merge($temp, [$key], array_keys($found));

                    /**
                     * @todo print log
                     */
                    Genetic::info(sprintf("Current : %s, lecturer : %s, room : %s, time : %s",
                        $lec['course']['classes']['code'], $lec['course']['lecturer']['name'],
                        $lec['room']['name'], $lec['time']['start']));

                    foreach ($found as $item){
                        Genetic::info(sprintf("Current : %s, lecturer : %s, room : %s, time : %s",
                            $item['course']['classes']['code'], $item['course']['lecturer']['name'],
                            $item['room']['name'], $item['time']['start']));
                    }
                    /* @todo end print log */
                }
            }
        }
        Genetic::info(sprintf("penalty : %d", $this->penalty[self::PENALTY_CLASS]));
    }

    public function penalty_morning_class(){
        Genetic::label("COUNT MORNING CLASS PENALTY");
        $this->penalty[self::PENALTY_MORNING_CLASS] = 0;
        foreach ($this->gen as $gen){
            if (isset($gen['course']) && $gen['course']['classes']['type'] == self::CLASS_TYPE_MORNING){
                $time = Carbon::createFromTimeString($gen['time']['start']);
                if ($this->midTime->diffInMinutes($time, false) > 0){
                    $this->penalty[self::PENALTY_MORNING_CLASS]++;

                    /* @todo print log */
                    Genetic::info(sprintf("Class : %s, type : %s, time %s",
                        $gen['course']['classes']['code'], $gen['course']['classes']['type'],
                        $gen['time']['start']));
                    /* @todo end print log */
                }
            }
        }
        Genetic::info(sprintf("penalty : %d", $this->penalty[self::PENALTY_MORNING_CLASS]));
    }

    public function penalty_afternoon_class(){
        Genetic::label("COUNT AFTERNOON CLASS PENALTY");
        $this->penalty[self::PENALTY_AFTERNOON_CLASS] = 0;
        foreach ($this->gen as $gen){
            if (isset($gen['course']) && $gen['course']['classes']['type'] == self::CLASS_TYPE_AFTERNOON){
                $time = Carbon::createFromTimeString($gen['time']['start']);
                if ($this->midTime->diffInMinutes($time, false) < 0){
                    $this->penalty[self::PENALTY_AFTERNOON_CLASS]++;

                    /* @todo print log */
                    Genetic::info(sprintf("Class : %s, type : %s, time %s",
                        $gen['course']['classes']['code'], $gen['course']['classes']['type'],
                        $gen['time']['start']));
                    /* @todo end print log */
                }
            }
        }
        Genetic::info(sprintf("penalty : %d", $this->penalty[self::PENALTY_AFTERNOON_CLASS]));
    }

    public function fitness(){
        $this->count_penalty();
        $this->fitness = 1 / (1 + $this->total_penalty);

        Genetic::info(sprintf("FITNESS : %f", $this->fitness));
        Genetic::line();
        Genetic::info("");
        Genetic::info("");
    }
}