<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pengampu extends Model
{
    public $timestamps = false;
    protected $fillable = ['lecturer_id','course_id','class_id'];

    public function lecturer(){
        return $this->belongsTo('App\Model\Lecturer');
    }

    public function classes(){
        return $this->belongsTo('App\Model\ClassRoom','class_id','id');
    }

    public function course(){
        return $this->belongsTo('App\Model\Course');
    }
}
