<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['code', 'name', 'semester', 'jumlah_sks', 'prodi_id'];

    public function prodi(){
        return $this->belongsTo('App\Model\Prodi');
    }
}
