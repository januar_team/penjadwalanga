<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TimeSlot extends Model
{
    public $timestamps = false;
    protected $fillable = ['start', 'end'];
}
