<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    public $timestamps = false;
    protected $fillable = ['id', 'name'];
}
