/*
SQLyog Enterprise v10.42 
MySQL - 5.5.5-10.1.10-MariaDB-log : Database - penjadwalan_ga
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`penjadwalan_ga` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `penjadwalan_ga`;

/*Table structure for table `classes` */

DROP TABLE IF EXISTS `classes`;

CREATE TABLE `classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `classes` */

insert  into `classes`(`id`,`code`,`type`,`created_at`,`updated_at`) values (1,'C17A','pagi','2018-05-23 06:08:17','2018-05-23 06:08:17'),(2,'C17B','sore','2018-05-23 06:08:46','2018-05-23 06:08:46'),(3,'C18A','pagi','2018-05-23 06:09:29','2018-05-23 06:09:29'),(4,'C18B','sore','2018-05-23 06:09:45','2018-05-23 06:09:45'),(5,'C17C','sore','2018-05-23 06:10:28','2018-05-23 06:10:28'),(6,'C16A','pagi','2018-05-23 06:10:55','2018-05-23 06:10:55'),(7,'C16B','sore','2018-05-23 06:11:08','2018-05-23 06:11:08'),(8,'C16C','sore','2018-05-23 06:11:21','2018-05-23 06:11:21');

/*Table structure for table `courses` */

DROP TABLE IF EXISTS `courses`;

CREATE TABLE `courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semester` int(11) NOT NULL,
  `jumlah_sks` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `courses` */

insert  into `courses`(`id`,`code`,`name`,`semester`,`jumlah_sks`,`created_at`,`updated_at`) values (1,'TINF-301','Bahasa Inggris I',1,2,'2018-05-23 01:05:36','2018-05-23 01:13:56'),(2,'TINF-244','Fisika',1,2,'2018-05-23 01:05:57','2018-05-23 01:05:57'),(3,'TINF-201','Kalkulus',1,3,'2018-05-23 01:14:33','2018-05-23 01:14:33'),(4,'TINF-246','Pengantar Teknik Informatika Komputer',1,2,'2018-05-23 01:14:51','2018-05-23 01:14:51'),(5,'TINF-245','Logika Informatika',1,2,'2018-05-23 01:15:10','2018-05-23 01:15:10'),(6,'TINF-102','Pendidikan Agama',1,2,'2018-05-23 01:15:31','2018-05-23 01:15:31'),(7,'TINF-211 + TINF-212','Algoritma & Pemrograman+Praktek',1,3,'2018-05-23 01:16:00','2018-05-23 01:16:00'),(8,'TINF-205','Bahasa Indonesia',1,2,'2018-05-23 01:16:17','2018-05-23 01:16:17'),(9,'TINF-299 + TINF-230','Jaringan Komputer I + Praktek',3,3,'2018-05-23 01:16:45','2018-05-23 01:16:45'),(10,'TINF-254 & TINF-255','Basis Data I + Praktek',3,3,'2018-05-23 01:17:02','2018-05-23 01:17:02'),(11,'TINF-243','Bahasa Inggris II',3,2,'2018-05-23 01:17:21','2018-05-23 01:17:21'),(12,'TINF-265 + TINF-266','Multimedia I + Praktek',3,3,'2018-05-23 01:17:50','2018-05-23 01:17:50'),(13,'TINF-344','Fisika II',3,2,'2018-05-23 01:19:05','2018-05-23 11:30:30'),(14,'TINF-267','Kecerdasan Buatan',3,3,'2018-05-23 01:19:25','2018-05-23 01:19:25'),(15,'TINF-233','Interaksi Manusia & Komputer',3,2,'2018-05-23 01:19:42','2018-05-23 01:19:42'),(16,'TINF-104','Filsafat Agama',3,2,'2018-05-23 01:20:01','2018-05-23 19:34:25');

/*Table structure for table `days` */

DROP TABLE IF EXISTS `days`;

CREATE TABLE `days` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `days` */

insert  into `days`(`id`,`name`) values (1,'Senin'),(2,'Selasa'),(3,'Rabu'),(4,'Kamis'),(5,'Jumat');

/*Table structure for table `lecturers` */

DROP TABLE IF EXISTS `lecturers`;

CREATE TABLE `lecturers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nidn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `lecturers` */

insert  into `lecturers`(`id`,`nidn`,`name`,`created_at`,`updated_at`) values (1,'1022028103','Ari Sellyana, S.T., M.Kom','2018-05-23 00:24:20','2018-05-23 00:38:50'),(2,'1020078801','Desyanti, M.Kom','2018-05-23 00:31:21','2018-05-23 00:31:21'),(3,'1024058902','Devit Satria, M.PdT','2018-05-23 00:39:13','2018-05-23 00:39:13'),(4,'1029089001','Erna Alimudin, S.T., M.Eng','2018-05-23 00:39:25','2018-05-23 00:39:25'),(5,'1014029101','Febrina Sari, M.Kom','2018-05-23 00:39:37','2018-05-23 00:39:37'),(6,'1025069001','Gellysa Urva, S.T., M.Kom','2018-05-23 00:39:51','2018-05-23 00:39:51'),(7,'1016118705','Nur Budi Nugraha, M.T','2018-05-23 00:41:34','2018-05-23 00:41:34'),(8,'1009098702','Mustazihim, M.Kom','2018-05-23 00:41:45','2018-05-23 00:41:45'),(9,'1007108702','Tri Handayani, M.T','2018-05-23 00:41:57','2018-05-23 00:41:57'),(10,'1025028403','Tri Yuliati, M.Kom','2018-05-23 00:42:10','2018-05-23 00:42:10'),(11,'1020088601','Merina Pratiwi, M.Si','2018-05-23 00:42:19','2018-05-23 00:42:19'),(12,'xxxxxxxxx','JULANOS S. ST','2018-05-23 11:24:56','2018-05-23 11:24:56'),(13,'xxxxxxxxx1','EMI YUZAR, M.Si','2018-05-23 11:28:57','2018-05-23 11:28:57'),(14,'xxxxxxxxx2','INDRA GUNAWAN, M.Ag','2018-05-23 11:37:58','2018-05-23 11:37:58'),(15,'xxxxxxxxx3','WENNY FITRIANY, M.Pd','2018-05-23 11:39:13','2018-05-23 11:39:13');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_05_22_123143_create_days_table',2),(4,'2018_05_22_123834_create_time_slots_table',2),(5,'2018_05_23_000416_create_lecturers_table',3),(6,'2018_05_23_000641_create_courses_table',3),(7,'2018_05_23_000902_create_classes_table',3),(8,'2018_05_23_061234_create_rooms_table',4),(9,'2018_05_23_063751_create_pengampus_table',5),(10,'2018_05_23_100541_alter_pengampu_add_unique_constraint',6);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `pengampus` */

DROP TABLE IF EXISTS `pengampus`;

CREATE TABLE `pengampus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lecturer_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `class_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pengampus_lecturer_id_course_id_class_id_unique` (`lecturer_id`,`course_id`,`class_id`),
  KEY `pengampus_course_id_foreign` (`course_id`),
  KEY `pengampus_class_id_foreign` (`class_id`),
  CONSTRAINT `pengampus_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`),
  CONSTRAINT `pengampus_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  CONSTRAINT `pengampus_lecturer_id_foreign` FOREIGN KEY (`lecturer_id`) REFERENCES `lecturers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `pengampus` */

insert  into `pengampus`(`id`,`lecturer_id`,`course_id`,`class_id`) values (22,1,7,1),(23,1,7,2),(25,1,8,2),(18,3,5,1),(19,3,5,2),(16,9,4,1),(17,9,4,2),(14,11,3,1),(15,11,3,2),(1,12,1,1),(2,12,1,2),(12,13,2,1),(13,13,2,2),(20,14,6,1),(21,14,6,2),(26,14,16,3),(27,14,16,4),(24,15,8,1);

/*Table structure for table `rooms` */

DROP TABLE IF EXISTS `rooms`;

CREATE TABLE `rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `floor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `rooms` */

insert  into `rooms`(`id`,`name`,`floor`,`created_at`,`updated_at`) values (1,'B1','1','2018-05-23 06:22:48','2018-05-23 06:22:48'),(2,'B2','2','2018-05-23 06:23:50','2018-05-23 06:23:50'),(3,'B9','1','2018-05-23 06:24:00','2018-05-23 06:24:00'),(4,'Ruang Rapat','3','2018-05-23 06:24:11','2018-05-23 06:24:11'),(5,'B15','2','2018-05-23 06:24:22','2018-05-23 06:24:22');

/*Table structure for table `time_slots` */

DROP TABLE IF EXISTS `time_slots`;

CREATE TABLE `time_slots` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `start` time NOT NULL,
  `end` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `time_slots` */

insert  into `time_slots`(`id`,`start`,`end`) values (1,'08:00:00','08:50:00'),(2,'08:50:00','09:40:00'),(3,'09:40:00','10:30:00'),(4,'10:30:00','11:20:00'),(5,'11:20:00','12:10:00'),(6,'12:10:00','13:00:00'),(7,'16:30:00','17:20:00'),(8,'17:20:00','18:10:00'),(9,'18:10:00','19:00:00'),(10,'19:00:00','19:50:00'),(11,'19:50:00','20:40:00'),(12,'20:40:00','21:30:00');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`) values (1,'Administrator','admin','admin@email.com','$2y$10$nJ7AaDaCVdcZMWfPo6mHie1Z9R6KsZQUXxMLlbzoSS.TaBU0KVGy2',NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
