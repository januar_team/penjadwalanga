@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Ruangan
            <small>Home</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Ruangan</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Daftar Ruangan</h3>
                        <div class="box-tools">
                            <a href="{{route('room.add')}}" class="btn btn-primary btn-xs bg-purple">
                                <i class="fa fa-plus"></i> tambah</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover">
                            <tbody><tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Lantai</th>
                                <th></th>
                            </tr>
                            <?php $no = ($page - 1 ) * 10 + 1; ?>
                            @foreach($rooms as $room)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$room->name}}</td>
                                    <td>{{$room->floor}}</td>
                                    <td>
                                        <a href="{{route('room.edit', ['id' => $room->id])}}?page={{$page}}">
                                            <span class="badge bg-blue">edit</span>
                                        </a>
                                        <a href="#"  class="btn-delete" data-id="{{$room->id}}">
                                            <span class="badge bg-red">hapus</span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        {{ $rooms->links() }}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete Item</h4>
                </div>
                <div class="modal-body">
                    Apakah Anda ingin menghapus item tersebut?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btn-delete-ok" class="btn btn-primary">Detele</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="application/javascript">
        jQuery(document).ready(function ($) {
            $('.btn-delete').click(function (event) {
                event.preventDefault();

                $('#btn-delete-ok').data("id", $(this).data('id'));
                $('#modalDelete').modal();
            });

            $('#btn-delete-ok').click(function (event) {
                document.location.href = '/room/delete/' + $(this).data('id');
            });
        });
    </script>
@endsection