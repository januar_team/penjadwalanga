@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Ruangan
            <small>Home</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('room')}}">Ruangan</a></li>
            <li class="active">Add</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Ruangan</h3>
                        <div class="box-tools">
                            <a href="{{route('room')}}" class="btn btn-primary btn-xs bg-purple">
                                <i class="fa fa-chevron-left"></i> kembali</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <form method="post">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label>Nama</label>
                                <input type="text" class="form-control" name="name" value="{{old('name')}}">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('floor') ? ' has-error' : '' }}">
                                <label>Lantai</label>
                                <input type="text" class="form-control" name="floor" value="{{old('floor')}}">
                                @if ($errors->has('floor'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('floor') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary bg-purple pull-right">Simpan</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection
