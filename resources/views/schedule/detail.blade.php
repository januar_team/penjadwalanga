@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Jadwal
            <small>Home</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Jadwal</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab1" data-toggle="tab" aria-expanded="true">Result</a>
                        </li>
                        <li class="">
                            <a href="#tab2" data-toggle="tab" aria-expanded="false">Log</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <th style="width:50%">Tahun ajaran:</th>
                                                <td>{{$schedule->tahun_ajaran}}</td>
                                            </tr>
                                            <tr>
                                                <th style="width:50%">Semester:</th>
                                                <td>{{$schedule->semester}}</td>
                                            </tr>
                                            <tr>
                                                <th style="width:50%">Populasi awal:</th>
                                                <td>{{$schedule->population}}</td>
                                            </tr>
                                            <tr>
                                                <th>Mutation Rate</th>
                                                <td>{{$schedule->mutation_rate}}</td>
                                            </tr>
                                            <tr>
                                                <th>Fitness</th>
                                                <td>{{$result->fitness}}</td>
                                            </tr>
                                            <tr>
                                                <th>Iterasi</th>
                                                <td>{{$schedule->iteration}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="schedule">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>
                                        </th>
                                        @foreach($days as $day)
                                            <th width="20%">{{$day->name}}</th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($times as $time)
                                        <tr>
                                            <td>{{$time->start}}</td>
                                            @foreach($days as $day)
                                                <?php
                                                $slot = array_filter($result->gen, function ($item) use ($day, $time) {
                                                    return isset($item['course']) && ($item['day']['id'] == $day->id) && ($item['time']['id'] == $time->id);
                                                });
                                                ?>
                                                <td>
                                                    @foreach($slot as $item)
                                                        <div class="panel panel-default panel-schedule">
                                                            <span class="bold">{{$item['course']['course']['name']}}</span>
                                                            <span>Kelas : {{$item['course']['classes']['code']}}</span>
                                                            <span>Dosen : {{$item['course']['lecturer']['name']}}</span>
                                                            <span>Ruangan : {{$item['room']['name']}}</span>
                                                        </div>
                                                    @endforeach
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab2">
                            <?php
                                if (file_exists($schedule->logs)){
                                    $logLines = file($schedule->logs);
                                    $logs = "";
                                    foreach($logLines as $line){
                                        $logs .= "<p>".$line."</p>";
                                    }
                                }else{
                                    $logs = "<p>File log is missing</p>";
                                }
                            ?>
                            {!! $logs !!}
                        </div>
                        <!-- /.tab-pane -->
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary bg-purple pull-right">Save Result</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="application/javascript">
        jQuery(document).ready(function ($) {
            $('#frm-generate').submit(function (event) {
                event.preventDefault();
                var frm = $(this);
                var data = $(this).serialize();
                var button = $(this).find('button[type=submit]').button('loading');
                $('.error-block > label.control-label').html('');
                $('.spinner').show();
                $.ajax({
                    url: frm.attr('action'),
                    type: 'post',
                    dataType: 'json',
                    data: data,
                })
                    .done(function (response) {
                        document.location.href = '/schedule/generate/' + response.key;
                    })
                    .fail(function (error) {
                        if (error.responseJSON != undefined) {
                            $('.error-block > label.control-label').html(error.responseJSON.message);
                        } else {
                            $('.error-block > label.control-label').html(error.statusText);
                        }
                    })
                    .always(function () {
                        $('.spinner').hide();
                        button.button('reset');
                    });

            });
        });
    </script>
@endsection
