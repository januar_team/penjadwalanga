@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Jadwal
            <small>Home</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Jadwal</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create Jadwal</h3>
                    </div>
                    <!-- /.box-header -->
                    <form id="frm-generate" method="post" action="{{route('schedule.generate')}}">
                        {{ csrf_field() }}

                        <div class="box-body">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Tahun Ajaran</label>
                                    <input type="text" class="form-control" id="tahun_ajaran" name="tahun_ajaran"
                                           required placeholder="Tahun ajaran"
                                           value="{{isset($tahun_ajaran)?$tahun_ajaran:""}}">
                                </div>
                                <div class="form-group">
                                    <label>Semester</label>
                                    <select class="form-control" id="semester" name="semester">
                                        <option value="0" {{isset($semester) ? (($semester == 0) ? "selected":"") : ""}}>
                                            Genap
                                        </option>
                                        <option value="1" {{isset($semester) ? (($semester == 1) ? "selected":"") : ""}}>
                                            Ganjil
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group has-error error-block">
                                    <label class="control-label"></label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group col-sm-6">
                                    <label>Populasi awal</label>
                                    <input type="number" min="10" step="1" class="form-control" required
                                           id="population" name="population" placeholder="Populasi awal"
                                           value="{{isset($population)?$population:"10"}}">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label>Jumlah generasi/ iterasi</label>
                                    <input type="number" min="0" step="1" class="form-control" id="generation"
                                           name="generation" placeholder="Jumlah generasi"
                                           value="{{isset($generation)?$generation:'100'}}">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label>Mutation Rate</label>
                                    <select class="form-control" id="mutation_rate" name="mutation_rate">
                                        <option value="0.01" {{isset($mutation_rate) ? (($mutation_rate == 0.01) ? "selected":"") : ""}}>
                                            0.01
                                        </option>
                                        <option value="0.02" {{isset($mutation_rate) ? (($mutation_rate == 0.02) ? "selected":"") : ""}}>
                                            0.02
                                        </option>
                                        <option value="0.03" {{isset($mutation_rate) ? (($mutation_rate == 0.03) ? "selected":"") : ""}}>
                                            0.03
                                        </option>
                                        <option value="0.04" {{isset($mutation_rate) ? (($mutation_rate == 0.04) ? "selected":"") : ""}}>
                                            0.04
                                        </option>
                                        <option value="0.05" {{isset($mutation_rate) ? (($mutation_rate == 0.05) ? "selected":"") : ""}}>
                                            0.05
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary bg-purple pull-right">Generate</button>
                        </div>

                    </form>

                    <div class="overlay spinner" style="display: none">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
                <!-- /.box -->
            </div>

            @if(isset($key))
                <div class="col-xs-12">
                    <form method="post" id="frm-save-result" action="{{route('schedule.generate', ['key' => $key])}}">
                        {{csrf_field()}}
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab1" data-toggle="tab" aria-expanded="true">Result</a>
                                </li>
                                <li class="">
                                    <a href="#tab2" data-toggle="tab" aria-expanded="false">Log</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab1">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tbody>
                                                    <tr>
                                                        <th style="width:50%">Populasi awal:</th>
                                                        <td>{{$population}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Mutation Rate</th>
                                                        <td>{{$mutation_rate}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Fitness</th>
                                                        <td>{{$result->fitness}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Iterasi</th>
                                                        <td>{{$iteration}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Execution Time</th>
                                                        <td>{{\Carbon\Carbon::now()->subSecond(round($exec_time))->diffForHumans(null, true)}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="schedule">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>
                                                </th>
                                                @foreach($days as $day)
                                                    <th width="20%">{{$day->name}}</th>
                                                @endforeach
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($times as $time)
                                                <tr>
                                                    <td>{{$time->start}}</td>
                                                    @foreach($days as $day)
                                                        <?php
                                                        $slot = array_filter($result->gen, function ($item) use ($day, $time) {
                                                            return isset($item['course']) && ($item['day']['id'] == $day->id) && ($item['time']['id'] == $time->id);
                                                        });
                                                        ?>
                                                    <td>
                                                        @foreach($slot as $item)
                                                            <div class="panel panel-default panel-schedule">
                                                                <span class="bold">{{$item['course']['course']['name']}}</span>
                                                                <span>Kelas : {{$item['course']['classes']['code']}}</span>
                                                                <span>Dosen : {{$item['course']['lecturer']['name']}}</span>
                                                                <span>Ruangan : {{$item['room']['name']}}</span>
                                                            </div>
                                                        @endforeach
                                                    </td>
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab2">
                                    <?php
                                    $logLines = file($logs);
                                    ?>
                                    @foreach($logLines as $line)
                                        <p>{{$line}}</p>
                                    @endforeach
                                </div>
                                <!-- /.tab-pane -->
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary bg-purple pull-right">Save Result</button>
                            </div>
                        </div>
                    </form>
                </div>
            @endif
        </div>
    </section>
@endsection

@section('script')
    <script type="application/javascript">
        jQuery(document).ready(function ($) {
            $('#frm-generate').submit(function (event) {
                event.preventDefault();
                var frm = $(this);
                var data = $(this).serialize();
                var button = $(this).find('button[type=submit]').button('loading');
                $('.error-block > label.control-label').html('');
                $('.spinner').show();
                $.ajax({
                    url: frm.attr('action'),
                    type: 'post',
                    dataType: 'json',
                    data: data,
                })
                    .done(function (response) {
                        document.location.href = '/schedule/generate/' + response.key;
                    })
                    .fail(function (error) {
                        if (error.responseJSON != undefined) {
                            $('.error-block > label.control-label').html(error.responseJSON.message);
                        } else {
                            $('.error-block > label.control-label').html(error.statusText);
                        }
                    })
                    .always(function () {
                        $('.spinner').hide();
                        button.button('reset');
                    });

            });
        });
    </script>
@endsection
