@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{$room}}</h3>

                        <p>Room</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$class}}</h3>

                        <p>Class</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{$lecturer}}</h3>

                        <p>Lecturer</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$course}}</h3>

                        <p>Courses</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Schedule</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="tbl-schedule" class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tahun Ajaran</th>
                                <th>Semester</th>
                                <th>Populasi</th>
                                <th>Generasi</th>
                                <th>Mutation Rate</th>
                                <th>Iterasi</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete Item</h4>
                </div>
                <div class="modal-body">
                    Apakah Anda ingin menghapus item tersebut?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btn-delete-ok" class="btn btn-primary">Detele</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
@endsection
@section('script')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap.js') }}"></script>
    <script>
        jQuery(document).ready(function ($) {
            var table = $('#tbl-schedule').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{{ route('schedule') }}",
                    "type": "POST",
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'tahun_ajaran', name: 'tahun_ajaran' },
                    { data: 'semester', 
						render: function(data, type, row, meta){
							return (data == 0) ? "genap" : "ganjil";
						}
					},
                    { data: 'population', name: 'population' },
                    { data: 'generation', name: 'generation' },
                    { data: 'mutation_rate', name: 'mutation_rate' },
                    { data: 'iteration', name: 'iteration' },
                    {
                        data: null,
                        render: function (data, type, row, meta) {
                            return  '<a href="/schedule/detail/' + data.id + '" data-toggle="tooltip" title="Detail" class="btn-detail badge bg-aqua"><i class="fa fa-list"></i> detail</a>' +
                                '<a href="#" class="btn-delete" data-id="'+ data.id +'"><span class="badge bg-red"><i class="fa fa-trash"></i> hapus</span></a>';
                        },
                        "orderable": false,
                    }
                ]
            });

            $('#tbl-schedule').on('click', '.btn-delete', function (event) {
                event.preventDefault();

                $('#btn-delete-ok').data("id", $(this).data('id'));
                $('#modalDelete').modal();
            });

            $('#btn-delete-ok').click(function (event) {
                var url = '/schedule/delete/' + $(this).data('id');
                var button = $(this).button('loading');
                $.ajax({
                    url : url,
                    type: 'POST',
                    dataType :'json',
                    success : function (response) {
                        if (response.status){
                            $('#modalDelete').modal('toggle');
                            table.draw();
                        }
                    },
                    error: function (response) {
                        alert("Failed to delete item");
                        console.log(response);
                    },
                    complete : function(){
                        button.button('reset');
                    }
                });
            });
        });
    </script>
@endsection