@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Mata Kuliah
            <small>Home</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Mata Kuliah</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Daftar Mata Kuliah</h3>
                        <div class="box-tools">
                            <a href="{{route('course.add')}}" class="btn btn-primary btn-xs bg-purple">
                                <i class="fa fa-plus"></i> tambah</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form id="frm-search" method="get" class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align: left">Program Studi</label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="prodi">
                                        <option value="">All</option>
                                        @foreach($prodis as $p)
                                            <option value="{{$p->id}}" {{($prodi && $prodi == $p->id) ? "selected" : ""}}>{{$p->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <tbody><tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Semester</th>
                                    <th>Jumlah SKS</th>
                                    <th>Program Studi</th>
                                    <th></th>
                                </tr>
                                <?php $no = ($page - 1 ) * 10 + 1; ?>
                                @foreach($courses as $course)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$course->code}}</td>
                                        <td>{{$course->name}}</td>
                                        <td>{{$course->semester}}</td>
                                        <td>{{$course->jumlah_sks}}</td>
                                        <td>{{($course->prodi)?$course->prodi->name:"-"}}</td>
                                        <td>
                                            <a href="{{route('course.pengampu', ['id' => $course->id])}}?{{"prodi=".app('request')->input('prodi')."&"}}page={{$page}}">
                                                <span class="badge bg-green">pengampu</span>
                                            </a>
                                            <a href="{{route('course.edit', ['id' => $course->id])}}?{{"prodi=".app('request')->input('prodi')."&"}}page={{$page}}">
                                                <span class="badge bg-blue">edit</span>
                                            </a>
                                            <a href="#" class="btn-delete" data-id="{{$course->id}}">
                                                <span class="badge bg-red">hapus</span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        {{ $courses->links() }}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete Item</h4>
                </div>
                <div class="modal-body">
                    Apakah Anda ingin menghapus item tersebut?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btn-delete-ok" class="btn btn-primary">Detele</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="application/javascript">
        jQuery(document).ready(function ($) {
            $('#frm-search').on('change', 'select[name="prodi"]', function (event) {
                $('#frm-search').submit();
            });

            $('.btn-delete').click(function (event) {
                event.preventDefault();

                $('#btn-delete-ok').data("id", $(this).data('id'));
                $('#modalDelete').modal();
            });

            $('#btn-delete-ok').click(function (event) {
                document.location.href = '/course/delete/' + $(this).data('id');
            });
        });
    </script>
@endsection