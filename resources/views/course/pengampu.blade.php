@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Pengampu
            <small>Mata Kuliah</small>
        </h1>
        <ol class="breadcrumb">
            <?php
                $param = app('request')->query();
                $query = array_map(function ($key, $item){
                    return "$key=$item";
                }, array_keys($param), array_values($param));
            ?>
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('course')}}?{{implode("&", $query)}}">Mata Kuliah</a></li>
            <li class="active">Pengampu</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Mata Kuliah : {{$course->name}}</h3>
                        <div class="box-tools">
                            <a href="{{route('course')}}?{{implode("&", $query)}}" class="btn btn-default btn-xs">
                                <i class="fa fa-chevron-left"></i> kembali</a>
                            <a id="btn-add" href="#" class="btn btn-primary btn-xs bg-purple">
                                <i class="fa fa-plus"></i> tambah</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover">
                            <tbody><tr>
                                <th>Nama Dosen</th>
                                <th>Kelas</th>
                                <th>Tipe</th>
                                <th></th>
                            </tr>
                            @foreach($pengampu as $p)
                                <tr>
                                    <td>{{$p->lecturer->name}}</td>
                                    <td>{{$p->classes->code}}</td>
                                    <td>{{$p->classes->type}}</td>
                                    <td>
                                        <a href="#" class="btn-edit" data-item="{{json_encode($p)}}">
                                            <span class="badge bg-blue">edit</span>
                                        </a>
                                        <a href="#" class="btn-delete" data-id="{{$p->id}}">
                                            <span class="badge bg-red">hapus</span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

    <div id="modal-pengampu" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Pengampu</h4>
                </div>
                <form id="frm-pengampu" method="post" action="{{route('course.pengampu', ['id' =>$course->id])}}">
                    <div class="modal-body">
                        <input type="hidden" name="course_id" value="{{$course->id}}">
                        <input type="hidden" name="id" value="">
                        <div class="form-group">
                            <label>Dosen</label>
                            <select name="lecturer_id" class="form-control">
                                @foreach($lecturer as $lec)
                                    <option value="{{$lec->id}}">{{$lec->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Kelas</label>
                            <select name="class_id" class="form-control">
                                @foreach($class as $cl)
                                    <option value="{{$cl->id}}">{{$cl->code}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div id="error_message" class="form-group">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Modal -->
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete Item</h4>
                </div>
                <div class="modal-body">
                    Apakah Anda ingin menghapus item tersebut?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btn-delete-ok" class="btn btn-primary">Detele</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="application/javascript">
        jQuery(document).ready(function ($) {
            $('#btn-add').click(function (event) {
                event.preventDefault();
                $('#modal-pengampu').modal();
                $('#frm-pengampu').find('input[name=id]').val("");
            });

            $('.btn-edit').click(function (event) {
                event.preventDefault();
                let data = $(this).data('item');
                $('#modal-pengampu').modal();
                $('#frm-pengampu').find('input[name=id]').val(data.id);
                $('#frm-pengampu').find('select[name=lecturer_id]').val(data.lecturer_id);
                $('#frm-pengampu').find('select[name=class_id]').val(data.class_id);
            });

            $('#frm-pengampu').submit(function (event) {
                event.preventDefault();
                let form = $(this);
                let data = $(this).serialize();
                let button = $(this).find('button[type=submit]').button('loading');
                form.find('div.form-group').removeClass('has-error');
                form.find('.help-block').remove();
                $.ajax({
                    url : $(this).attr('action'),
                    type: 'POST',
                    dataType :'json',
                    data : data,
                    success : function (response) {
                        if (response.status){
                            $('#modal-pengampu').modal('toggle');
                            document.location.reload()
                        }
                    },
                    error: function (response) {
                        if (response.readyState == 4) {
                            if(response.status === 422 || response.status === 423){
                                var errors = response.responseJSON.errors;
                                $.each(errors, function(key, error){
                                    var item = form.find('input[name='+ key +']');
                                    item = (item.length > 0) ? item : form.find('select[name='+ key +']');
                                    item = (item.length > 0) ? item : form.find('textarea[name='+ key +']');
                                    item = (item.length > 0) ? item : form.find("input[name='"+ key +"[]']");
                                    item.parent().addClass('has-error');
                                    item.parent().append('<span class="help-block">'+ error +'</span>');
                                })
                            }else if(response.status === 400){
                                form.find('#error_message').addClass('has-error');
                                form.find('#error_message').append('<span class="help-block">'+response.responseJSON.message+'</span>');
                            } else {
                                form.find('#error_message').addClass('has-error');
                                form.find('#error_message').append('<span class="help-block">Whoops, looks like something went wrong.</span>');
                            }
                        } else if(response.readyState == 0){
                            form.find('#error_message').addClass('has-error');
                            form.find('#error_message').append('<span class="help-block">Request timeout, please check your connection</span>');
                        }
                    },
                    complete : function(){
                        button.button('reset');
                    }
                });
            });

            $('.btn-delete').click(function (event) {
                event.preventDefault();

                $('#btn-delete-ok').data("id", $(this).data('id'));
                $('#modalDelete').modal();
            });

            $('#btn-delete-ok').click(function (event) {
                document.location.href = '/course/pengampu/{{$course->id}}/delete/' + $(this).data('id') + '?{!!implode("&", $query)!!}';
            });
        });
    </script>
@endsection
