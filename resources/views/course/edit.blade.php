@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Mata Kuliah
            <small>Home</small>
        </h1>
        <ol class="breadcrumb">
            <?php
            $param = app('request')->query();
            $query = array_map(function ($key, $item){
                return "$key=$item";
            }, array_keys($param), array_values($param));
            ?>
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('course')}}?{{implode("&", $query)}}">Mata Kuliah</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Mata Kuliah</h3>
                        <div class="box-tools">
                            <a href="{{route('course')}}?{{implode("&", $query)}}" class="btn btn-primary btn-xs bg-purple">
                                <i class="fa fa-chevron-left"></i> kembali</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <form method="post">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                                <label>Kode</label>
                                <input type="text" class="form-control" name="code" value="{{old('code', $course['code'])}}">
                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label>Nama</label>
                                <input type="text" class="form-control" name="name" value="{{old('name', $course['name'])}}">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('semester') ? ' has-error' : '' }}">
                                <label>Semester</label>
                                <select class="form-control" name="semester">
                                    @for($i = 1; $i <= 8; $i++)
                                        <option value="{{$i}}" {{old('semester', $course['semester']) == $i ? "selected" : ""}}>{{$i}}</option>
                                    @endfor
                                </select>
                                @if ($errors->has('semester'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('semester') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('jumlah_sks') ? ' has-error' : '' }}">
                                <label>Jumlah SKS</label>
                                <input type="number" class="form-control" name="jumlah_sks" value="{{old('jumlah_sks', $course['jumlah_sks'])}}">
                                @if ($errors->has('jumlah_sks'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('jumlah_sks') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('prodi_id') ? ' has-error' : '' }}">
                                <label>Program Studi</label>
                                <select class="form-control" name="prodi_id" value="{{old('prodi_id')}}">
                                    @foreach($prodi as $p)
                                        <option value="{{$p->id}}" {{old('prodi_id', $course['prodi_id']) == $p->id ? "selected" : ""}}>{{$p->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('prodi_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('prodi_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary bg-purple pull-right">Simpan</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="application/javascript">
        jQuery(document).ready(function ($) {
            @if(old('prodi_id', $course['prodi_id']) == null)
                $('select[name="prodi_id"]').prop('selectedIndex', -1);
            @endif
        });
    </script>
@endsection