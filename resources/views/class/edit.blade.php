@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Kelas
            <small>Home</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('class')}}?page={{$page}}">Kelas</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Kelas</h3>
                        <div class="box-tools">
                            <a href="{{route('class')}}?page={{$page}}" class="btn btn-primary btn-xs bg-purple">
                                <i class="fa fa-chevron-left"></i> kembali</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <form method="post">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                                <label>Kode</label>
                                <input type="text" class="form-control" name="code" value="{{old('code', $class['code'])}}">
                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                <label>Tipe</label>
                                <select class="form-control" name="type">
                                    <option value="pagi" {{old('type', $class['type']) == 'pagi' ? "selected" : ""}}>pagi</option>
                                    <option value="sore" {{old('type', $class['type']) == 'sore' ? "selected" : ""}}>sore</option>
                                </select>
                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary bg-purple pull-right">Simpan</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection
